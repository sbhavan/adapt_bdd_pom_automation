package com.adapt.qa.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src\\main\\java\\com\\adapt\\qa\\features\\"},
		glue = {"com\\adapt\\qa\\stepdefinitions"},
		//format= {"pretty", "html:test-output", "json:report_json/cucumber.json", "junit:report_"}	
		dryRun = false,
		monochrome = true,
		strict = true
		//tags = {"@Smoke"}
		)

public class testRunner {

	
	
}
