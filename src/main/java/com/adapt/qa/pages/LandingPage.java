package com.adapt.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.adapt.qa.base.AdaptBase;
import com.adapt.qa.pages.*;
public class LandingPage extends AdaptBase {
	
	
	@FindBy(xpath="//button[@type='button']")
	WebElement sideMenu;
	
	@FindBy(xpath="//i[@class='zmdi zmdi-home']")
	WebElement homeButton;	
	
	@FindBy(xpath="//button[@type='title']")
	WebElement helpIcon;
	
	@FindBy(xpath="//i[@class='zmdi zmdi-settings']")
	WebElement settingsIcon;
	
	@FindBy(xpath="//i[@class='zmdi zmdi-notifications']")
	WebElement notificationsIcon;
	
	
	
	public LandingPage() {				
		PageFactory.initElements(driver, this);
	}
	
	public String validateLandingPageTitle(){
		return driver.getTitle();
	}
	
	public boolean validateSideMenu() {
		return sideMenu.isDisplayed();
	}
	
	public boolean validatehomeButton() {
		return homeButton.isDisplayed();
	}
	
	public boolean validateHelpIcon() {
		return helpIcon.isDisplayed();
	}	
	
	public boolean validatesettingsIcon() {
		return settingsIcon.isDisplayed();
	}
	
	public boolean validatenotificationsIcon() {
		return notificationsIcon.isDisplayed();
	}
	
}
