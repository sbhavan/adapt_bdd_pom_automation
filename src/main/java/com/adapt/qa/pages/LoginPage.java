package com.adapt.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.adapt.qa.base.AdaptBase;

public class LoginPage extends AdaptBase {
	
	
	//Page Factory - OR:
			@FindBy(id="username")
			WebElement username;
			
			@FindBy(id="password")
			WebElement password;
			
			@FindBy(xpath="//button[@type='submit']")
			WebElement login;				
			
			@FindBy(xpath="//img[contains(@src,'/static/media/Adapt_logo.68972c1e.png')]")
			WebElement adaptLogo;
			
			@FindBy(xpath="//div[@class='text']")
			WebElement textSlogan;
			
			//@FindBy(xpath="//img[contains(@src,'/static/media/analyze_data.c0b37616.svg')]")
			//WebElement customLogo;
			
			@FindBy(xpath="//div[@class='welcomeText']")
			WebElement welcomeText;
			
			
			public LoginPage() {				
				PageFactory.initElements(driver, this);
			}
			
			public String validateLoginPageTitle(){
				return driver.getTitle();
			}
			
			public boolean validateadaptLogo() {
				return adaptLogo.isDisplayed();
			}
			
			public String validatetextSlogan(){
				return textSlogan.getText();
			}
			
			//public boolean validatecustomLogo() {
				//return customLogo.isDisplayed();
			//}
			
			public String validatewelcomeText(){
				return welcomeText.getText();
			}
			
			
			public LandingPage login(String un, String pwd) {
				username.sendKeys(un);
				password.sendKeys(pwd);
				login.click();
				
				return new LandingPage();				
			}	

}
