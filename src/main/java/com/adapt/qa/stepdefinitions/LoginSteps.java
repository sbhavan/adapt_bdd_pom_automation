package com.adapt.qa.stepdefinitions;

import com.adapt.qa.base.AdaptBase;
import com.adapt.qa.pages.LandingPage;
import com.adapt.qa.pages.LoginPage;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps extends AdaptBase{
	
	LoginPage loginPage;
	LandingPage landingPage;
	

	@Before
	public void setup() {
		initialization();				
	}
	
		
	@Given("^User is on Adapt Cloud Login Page$")
	public void user_is_on_Adapt_Cloud_Login_Page() {		
		driver.get(prop.getProperty("url"));	
		
		loginPage = new LoginPage();
	}
	
	@When("^Adapt's Login page Title is BFC - AWS Dashboard$")
	public void adapt_login_page_title_is_bfc_aws_dashboard() {	
		String title = loginPage.validateLoginPageTitle();	
		org.junit.Assert.assertEquals(title, "BFC - AWS Dashboard");
	}
	
	@Then("^user enter username and password$")
	public void user_enter_username_and_password() {	
		landingPage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
	}
//	
//	@And("^user clicks on Login button$")
//	public void user_clicks_on_login_button() {		
//	}
//	
//	@Then("^user successfully logged in to Adapt$")
//	public void user_successfully_logged_in_to_adapt() {	
//		System.out.println("Print");
//	}
	
	@After
	public void tearDown() {
		driver.quit();
	}
}
